package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        String keepPlaying;

        do {
            System.out.print("Let's play round " + roundCounter + "\n");
            boolean validChoice = false;
            String humanChoice;

            do {
                System.out.print("Your choice (Rock/Paper/Scissors)?");
                humanChoice = readInput("");
                
                if (rpsChoices.contains(humanChoice)){
                    validChoice = true;
                }
                else {
                    System.out.print("I do not understand " + humanChoice + ". Could you try again?\n");
                }
            } while (!validChoice);

            Random random = new Random();
            int computer = random.nextInt(rpsChoices.size());
            String computerChoice = rpsChoices.get(computer);
            String winner;

            if (humanChoice.equals("scissors") && computerChoice == "paper"){
                winner = "Human wins!";
                humanScore++;
            }
            else if (humanChoice.equals("rock") && computerChoice == "scissors"){
                winner = "Human wins!";
                humanScore++;
            }
            else if (humanChoice.equals("paper") && computerChoice == "rock"){
                winner = "Human wins!";
                humanScore++;
            }
            else if (humanChoice.equals(computerChoice)){
                winner = "It's a tie!";
            }
            else {
                winner = "Computer wins!";
                computerScore++;
            }

            System.out.print("Human chose " + humanChoice + ", computer chose " + computerChoice + ". " + winner + "\n");
            System.out.print("Score: human " + humanScore + ", computer " + computerScore + "\n");
            System.out.print("Do you wish to continue playing? (y/n)?");

            keepPlaying = readInput("");
            roundCounter++; 

        } while (keepPlaying.equals("y"));

        System.out.print("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
